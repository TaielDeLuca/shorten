<?php 

	class FileEngine{
		public $encName;

		function genEncName(){
			$random_num = uniqid();
            $current_time = date('d_m_y_h_i_s');
            $unencrypt_name = "{$random_num}_{$current_time}";
            return md5($unencrypt_name);
		}

		function checkFile($file,$size){
			return ($file['size']>$size);
		}

		function handleFile($file){
            $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
            $this->encName = $this->genEncName().'.'.$ext;
            $final_path = FILE_FOLDER.$this->encName;
            if(move_uploaded_file($file['tmp_name'],$final_path)){
                return $this->encName;
            }
            return false;
		}
	}



?>