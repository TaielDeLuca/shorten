<?php

class Database
{
	public $conexion;
	function __construct()
	{
		$this->makeConnection();
	}
	public function makeConnection()
	{
		$this->conexion = new mysqli(
			$_ENV['DB_HOST'],
			$_ENV['DB_USERNAME'],
			$_ENV['DB_PASSWORD'],
			$_ENV['DB_NAME'],
		);
	}
}
?>