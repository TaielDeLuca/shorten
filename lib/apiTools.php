<?php

function success($data)
{
    response(200, $data);
    die();
}

function badRequest()
{
    response(400, ['error' => "Bad Request"]);
    die();
}


function getMethod()
{
    return $_SERVER['REQUEST_METHOD'];
}

function requirePost()
{
    if (getMethod() != 'POST') {
        badRequest();
    }
}

function requireGet()
{
    if (getMethod() != 'GET') {
        badRequest();
    }
}

function requirePut()
{
    if (getMethod() != 'PUT') {
        badRequest();
    }
}

function requireUpdate()
{
    if (getMethod() != 'UPDATE') {
        badRequest();
    }
}


function verifyAutorization()
{
    $headers = apache_request_headers();
    if (!isset($headers['Authorization'])) {
        response(403, "Prohibido");
    }
    try {
        $jwt_token = substr($headers["Authorization"], 7);
        $data = JWT::decode($jwt_token, new Key($_ENV["JWT_KET"], 'HS256'));
    } catch (Exception $e) {
        response(403, "Prohibido");
    }
}
function verifyData($dataArray, $dataNeeded)
{
    global $method;
    if ($method == 'PUT') {
        foreach ($dataNeeded as $key => $value) {
            if (!isset($dataArray->$value)) {
                badRequest();
            }
        }
        return true;
    }
    foreach ($dataNeeded as $key => $value) {
        if (!isset($dataArray[$value])) {
            badRequest();
        }
    }
    return true;
}


function response($status, $dataArray)
{
    http_response_code($status);
    echo $dataArray;
    die();
}
?>