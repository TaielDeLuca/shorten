<?php
session_start();

// Cabeceras para hacer que la API sea pública
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
header("Content-Type: application/json");

//Levanta las configuraciones
require_once ('vendor/autoload.php');
//Levanta la base de datos
require 'lib/Database.php';
require 'lib/EmailEngine.php';
require 'lib/WppEngine.php';
require 'lib/TplEngine.php';
require 'lib/FilesEngine.php';




$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();



$url = array_values(array_filter(explode('/', $_SERVER['REQUEST_URI'])));//Url filtrada y con la url defecto eliminada
$method = $_SERVER['REQUEST_METHOD']; //Metodo con el que se quizo ingresar


//Levanta funciones de las api
require 'lib/apiTools.php';
//Modelo
if (isset($url[1])) {
	$model_requested = ucfirst($url[1]);
	if ($model_requested != "Auth") {
		verifyAutorization();
	}
	$url_model = "model/{$model_requested}Model.php";

	if (file_exists($url_model)) {

		//Incluye el controlador
		require $url_model;

		$modelo = new $model_requested;

		if (isset($url[2])) {
			$method_requested = $url[2];
			if (method_exists($modelo, $method_requested)) {

				switch ($method) {
					case 'GET': // obtener
						unset($url[0]);
						unset($url[1]);
						unset($url[2]);
						$parameters = array_values($url);
						break;

					case 'POST': // colocar
						$parameters = $_POST;
						break;

					case 'PUT': // actualizar

						$_PUT = json_decode(file_get_contents("php://input"));

						$parameters = $_PUT;

						break;

					case 'DELETE': // borrar
						unset($url[0]);
						unset($url[1]);
						unset($url[2]);
						$parameters = array_values($url);
						break;
				}
				$response = $modelo->$method_requested($parameters);

				echo json_encode($response);
				die();
			} else {
				response(404, ['error' => 'No existe esa funcionalidad']);
			}
		}
	} else {
		response(404, ['error' => 'No se encontro esa api']);
	}
} else {
	response(404, ['error' => 'No se ingreso ninguna api']);
}


?>