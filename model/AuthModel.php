<?php
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class Auth extends Database
{

    function login($parameters)
    {
        requirePost();
        verifyData($parameters, ['email', 'password']);

        $this->email = $parameters['email'];
        $this->password = md5($parameters['password']);

        $sql = "SELECT 
        `idToken`
        FROM `users`
        WHERE 
        `email`=? and 
        `password`=? and 
        blocked = 0 and 
        active =1 and `reset` = 0";

        $query = $this->conexion->prepare($sql);
        $query->bind_param("ss", $this->email, $this->password);
        $query->bind_result($idToken);
        $query->execute();
        $query->fetch();

        if ($idToken) {
            $token = JWT::encode(['id' => $idToken], $_ENV["JWT_KET"], 'HS256');
            response(200, $token);
        }
        badRequest();
    }




    function autenticated()
    {
        requireGet();
        $headers = apache_request_headers();
        if (!isset($headers['Authorization'])) {
            return false;
        }
        try {
            $jwt_token = substr($headers["Authorization"], 7);
            $data = JWT::decode($jwt_token, new Key($_ENV["JWT_KET"], 'HS256'));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }










}
?>